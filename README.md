# Sign Translator

A sign translator created in React with the purpose of learning the framework.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)

## Install

1. Clone this repository.
2. Navigate to the folder via terminal.
3. Execute `npm install` in terminal.

## Usage

1. Create a file with the name `.env` in the root directory.
2. Add the following lines to the file:
`REACT_APP_API_KEY=<your key>`
`REACT_APP_API_URL=<your url>`
3. Save the file.
4. Execute `npm start` in terminal.
5. Runs the app in the development mode.
6. Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.

Live version can be found [here](https://od-hu-assignment-two.herokuapp.com/).

## Maintainers

[Huwaida Al Hamdawee (@huwaida-al)](https://gitlab.com/huwaida-al)
[Oliver Dahlqvist (@OliverDahlqvist)](https://gitlab.com/OliverDahlqvist)

## Contributing

If you have any sugguestions then feel free to open an issue.