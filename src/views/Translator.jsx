import TranslationInput from "../components/TranslationInput"
import withAuth from "../hoc/withAuth"

const App = () => {
    return (
        <TranslationInput></TranslationInput>
    )
}
export default withAuth(App)