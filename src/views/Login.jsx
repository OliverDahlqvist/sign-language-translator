import Card from "../components/Card"
import LoginForm from "../components/LoginForm"

const Login = () => {
    return(
        <Card>
            <LoginForm/>
        </Card>
    )
}
export default Login