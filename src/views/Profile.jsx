import ProfileCard from "../components/ProfileCard"
import withAuth from "../hoc/withAuth"
import TranslationHistory from "../components/TranslationHistory"

const Profile = () => {
    return (
        <>
            <ProfileCard>
                <TranslationHistory></TranslationHistory>
            </ProfileCard>
        </>
    )
}
export default withAuth(Profile)