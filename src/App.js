import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import './App.css';
import Header from './components/Header.jsx';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Login from './views/Login'
import Profile from './views/Profile';
import Translator from './views/Translator';

function App() {
  return (
    <BrowserRouter>
      <Header/>
      <div style={{position: "absolute", minHeight: "50%", minWidth: "100%"}} className="gradient-background">

      </div>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/profile" element={<Profile />} />
        <Route path="/translator" element={<Translator />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;