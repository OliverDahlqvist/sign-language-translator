import React, { useState } from 'react';
import { MDBBtn, MDBCardTitle, MDBInputGroup } from 'mdb-react-ui-kit';
import { useForm } from 'react-hook-form';
import { loginUser } from '../api/user';
import { useNavigate } from 'react-router-dom';
import { storageSave } from '../utils/storage';
import { useEffect } from 'react';
import { STORAGE_KEY_USER } from '../const/storageKeys';
import { useUser } from '../context/UserContext';
import { BarLoader } from 'react-spinners';

const usernameConfig = {
    required: true,
    minLength: 4
}

const LoginForm = () => {
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm()
    const { user, setUser } = useUser();
    const navigate = useNavigate()
    const [loading, setLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    useEffect(() => {
        if (user !== null) {
            navigate('/translator')
        }
    }, [user, navigate])

    // Called when clicking on login, attempts to login with the given username.
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, userResponse] = await loginUser(username);
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse);
            setUser(userResponse)
        }
        if (error) {
            setErrorMessage(error.message);
            setLoading(false);
        }
    }
    // Used to display error messages from input
    const displayErrorMessage = (() => {
        if (!errors.username)
            return null
        if (errors.username.type === "minLength") {
            return <>Min length: {usernameConfig.minLength}</>
        }
    })()

    return (
        <>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '20px' }}>
                <img width={"40%"} src='./images/logo-hello.png' alt='...'></img>
                <MDBCardTitle>Welcome to Lost in Translation!</MDBCardTitle>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <MDBInputGroup className='mb-3'>
                    <input className='form-control' placeholder="What's your name" type="text" {...register("username", usernameConfig)} />
                    <MDBBtn disabled={loading} className="btn btn-primary gradient-primary">Login</MDBBtn>
                </MDBInputGroup>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginBottom: '0px' }}>
                    <BarLoader cssOverride={{ position: 'absolute' }} loading={loading} color="rgba(166, 193, 238, 0.5)" />
                </div>
                <label className="form-label">{displayErrorMessage}{errorMessage}</label>
            </form>
        </>
    );
}
export default LoginForm