import { MDBBtn, MDBCardText, MDBListGroup, MDBListGroupItem } from 'mdb-react-ui-kit';
import { useEffect, useState } from 'react';
import { BarLoader } from 'react-spinners';
import { createHeaders } from '../api';
import { apiURL, STORAGE_KEY_USER } from '../const/storageKeys';
import { storageRead, storageSave } from '../utils/storage';

const App = () => {
    const [displayHistory, setDisplayHistory] = useState([]);
    const [loading, setLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [historyCleared, setHistoryCleared] = useState(false);

    const user = storageRead(STORAGE_KEY_USER)
    const userHistory = user.translations

    useEffect(() => {
        renderHistory(userHistory);
    }, []);


    // Sets state of displayHistory to the users
    // translation history by creating new group items.
    const renderHistory = (translations) => {
        let result = []
        for (let i = 0; i < translations.length; i++) {
            result.push(<MDBListGroupItem key={i}>{translations[i]}</MDBListGroupItem>)
        }
        setDisplayHistory(result)
    }

    // Attempts to clear the history in the database.
    const clearHistory = async () => {
        const translationLength = storageRead(STORAGE_KEY_USER).translations.length
        if (translationLength <= 0) {
            return;
        }

        user.translations = []
        setLoading(true);
        try {
            const response = await fetch(`${apiURL}/${user.id}`, {
                method: 'PATCH',
                headers: createHeaders(),
                body: JSON.stringify({
                    translations: []
                })
            });
            if (!response.ok) {
                throw new Error('Could not update translation history');
            }
            const data = await response.json();
            renderHistory(data.translations);
            storageSave(STORAGE_KEY_USER, user)
            setLoading(false);
            setErrorMessage("");
            setHistoryCleared(true);
        }
        catch (error) {
            setLoading(false);
            setErrorMessage("Couldn't clear history");
        }
    }

    // Displays a proper message depending on the translation history
    const displayHistoryMessage = (() => {
        const length = userHistory.length;
        if (length === 0){
            if(historyCleared)
                return "Your translation history has been cleared"
            return ("Your translation history is empty")
        }
        else
            return (`Your translation history consists of ${length} sentence${length > 1 ? "s" : ""}`)
    })()

    const displayClearButtonMessage = (() => {
        if(historyCleared || userHistory.length <= 0)
        return "No history to clear"
        else
        return "Clear history"
    })()

    return (
        <>
            <MDBCardText className='text-center'>{displayHistoryMessage}</MDBCardText>
            <MDBListGroup flush style={{ minWidth: '22rem' }}>
                {displayHistory}
            </MDBListGroup>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <MDBBtn onClick={clearHistory} disabled={loading || historyCleared || userHistory.length <= 0} className='btn btn-primary gradient-primary'>{displayClearButtonMessage}</MDBBtn>
            </div>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '10px', marginBottom: '0px' }}>
                <BarLoader cssOverride={{position: 'absolute'}} loading={loading} color="rgba(166, 193, 238, 0.5)" />
                <p>{errorMessage}</p>
            </div>
        </>
    )
}

export default App