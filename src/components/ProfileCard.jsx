import { MDBCardTitle } from 'mdb-react-ui-kit';
import Card from '../components/Card';
import { STORAGE_KEY_USER } from '../const/storageKeys';
import { storageRead } from '../utils/storage';

const App = ({ children }) => {
  const user = storageRead(STORAGE_KEY_USER)
  if (user == null) {

  }
  return (
    <Card>
      <div style={{ display: 'flex', justifyContent: "center", alignItems: "center", marginBottom: '20px' }}>
        <img width={"30%"} src='./images/robot.png' alt='...'></img>
      </div>
      <div style={{ display: 'flex', justifyContent: "center", alignItems: "center", marginBottom: '10px' }}>
        <MDBCardTitle>Welcome {user.username}!</MDBCardTitle>
      </div>

      {children}
    </Card>
  );
}

export default App