import React from 'react';
import { MDBCard, MDBCardBody } from 'mdb-react-ui-kit';
import 'animate.css';

const Card = (props) => {
    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '40px' }} className="animate__animated animate__fadeInUp">
            <MDBCard style={{ maxWidth: '40rem' }}>
                <MDBCardBody>
                    {props.children}
                </MDBCardBody>
            </MDBCard>
        </div>
    );
}

export default Card;