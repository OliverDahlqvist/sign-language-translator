import React, { useState } from 'react';
import { MDBBtn, MDBCardText, MDBInputGroup } from "mdb-react-ui-kit"
import Card from "./Card"
import { useForm } from 'react-hook-form';
import { storageRead, storageSave } from '../utils/storage';
import { STORAGE_KEY_USER } from '../const/storageKeys';
import { createHeaders } from '../api';
import { BarLoader } from 'react-spinners';

const apiUrl = process.env.REACT_APP_API_URL
const inputConfig = {
    required: false,
    maxLength: 40
}

const App = () => {
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm()

    // Called when submitting translation
    const onSubmit = ({ input }) => {
        if(input.length <= 0) return;
        translate(input);
        addHistory(input);
    }
    const [currentImages, setImages] = useState([]);
    const [loading, setLoading] = useState(false);
    const user = storageRead(STORAGE_KEY_USER);

    const translations = user.translations;

    // Gets the sentence from the form sets the image array with new img elements
    const translate = (sentence) => {
        const items = [];
        sentence = sentence.toLowerCase();
        for (let i = 0; i < sentence.length; i++) {
            let char = sentence[i];
            if (!(/^[a-zA-Z]+$/).test(char))
                if(char === " ")
                    char = 0;
                else
                    continue;
            items.push(<img className='translationImage' key={i} src={`images/${char}.png`} alt={sentence[i]}></img>);
        }
        setImages(items)
    }
    
    /**
     * Updates database with the new sentence and saves it to storage
     * @param {string} input The sentence to be added to history
     * @returns Error and new user
     */
    const addHistory = async (input) => {
        if(translations.length >= 10){
            translations.shift();
        }
        translations.push(input);
        user.translations = translations;
        setLoading(true);

        try {
            const response = await fetch(`${apiUrl}/${user.id}`, {
                method: 'PATCH',
                headers: createHeaders(),
                body: JSON.stringify({
                    translations: translations
                })
            });
            if (!response.ok) {
                throw new Error('Could not update translation history');
            }
            const data = await response.json();
            setLoading(false);
            storageSave(STORAGE_KEY_USER, user)
            return [null, data]
        }
        catch (error) {
            setLoading(false);
        }
    }

    const displayErrorMessage = (() => {
        if(!errors.input){
            return null
        }
        if (errors.input.type === "maxLength") {
            return <>Max sentence length: {inputConfig.maxLength}</>
        }
    })()

    return (
        <Card>
            <MDBCardText>Please enter a sentence to translate to sign language</MDBCardText>
            <form onSubmit={handleSubmit(onSubmit)}>
                <MDBInputGroup className='mb-3'>
                    <input className='form-control' placeholder="" type="text" {...register("input", inputConfig)} />
                    <MDBBtn className="gradient-primary">Translate</MDBBtn>
                </MDBInputGroup>
            </form>
            <div>
                {currentImages}
            </div>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '10px', marginBottom: '0px' }}>
                <p>{displayErrorMessage}</p>
                <BarLoader cssOverride={{ position: 'absolute'}} loading={loading} color="rgba(166, 193, 238, 0.5)" />
            </div>
        </Card>
    )
}
export default App