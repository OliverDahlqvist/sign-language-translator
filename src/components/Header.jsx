import React from 'react';
import {
  MDBNavbar,
  MDBContainer,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBBtn,
} from 'mdb-react-ui-kit';
import { NavLink, useNavigate } from 'react-router-dom';
import { useUser } from '../context/UserContext';
import { useEffect } from 'react';
import { useState } from 'react';
import { storageDelete } from '../utils/storage';
import { STORAGE_KEY_USER } from '../const/storageKeys';

export default function App() {
  const { user, setUser } = useUser();
  const [visibility, setVisibility] = useState({});

  useEffect(() => {
    const visibility = user === null ? { visibility: 'hidden' } : { visibility: 'visible' }
    setVisibility(visibility);
  }, [user])
  const navigate = useNavigate()
  const signOut = () => {
    setUser(false);
    storageDelete(STORAGE_KEY_USER)
    navigate(0);
  }

  return (
    <MDBNavbar expand='lg' light bgColor='light' className="gradient-custom">
      <MDBContainer fluid>
        <nav aria-label='breadcrumb'>
          <MDBBreadcrumb>
            <MDBBreadcrumbItem>
              <NavLink to="/Translator">Lost in Translation</NavLink>
            </MDBBreadcrumbItem>
            <MDBBreadcrumbItem style={visibility}>
              <NavLink to="/Profile">Profile</NavLink>
            </MDBBreadcrumbItem>
            <MDBBreadcrumbItem style={visibility} active aria-current='page'>
              <MDBBtn className='btn-sm btn btn-light' onClick={signOut}>Sign out</MDBBtn>
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
        </nav>
      </MDBContainer>
    </MDBNavbar>
  );
}