import { createHeaders } from "./index";
const apiUrl = process.env.REACT_APP_API_URL

/**
 * Takes a username and check if it exists in the database.
 * @param {string} username Input username.
 * @returns Errors and user.
 */
const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`);
        if (!response.ok) {
            throw new Error('Could not complete request.');
        }
        const data = await response.json();
        return [null, data]
    }
    catch (error) {
        return [error, null]
    }
}

/**
 * Attempts to create a new user in the database.
 * @param {string} username 
 * @returns Errors and user.
 */
const createUser = async (username) => {
    try {
        const response = await fetch(apiUrl, {
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        });
        if (!response.ok) {
            throw new Error('Could not create user with username');
        }
        const data = await response.json();
        return [null, data]
    }
    catch (error) {
        return [error, null]
    }
}

// Checks if a user with the given name already exists, creates a new one if one doesn't exist.
export const loginUser = async username => {
    const [checkError, user] = await checkForUser(username);
    if (checkError !== null) {
        return [checkError, null]
    }

    if (user.length > 0) {
        return [null, user.pop()];
    }
    
    return await createUser(username);
}